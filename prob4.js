function problem4(obj1){
    if(obj1["middle_name"]){
        return (obj1["first_name"]+" "+obj1["middle_name"]+" "+obj1["last_name"]);
    }
    else{
        return (obj1["first_name"]+" "+obj1["last_name"]);
    }
}
module.exports=problem4;